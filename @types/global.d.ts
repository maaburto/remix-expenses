import type { PrismaClient } from "@prisma/client";

declare global {
  var __db: PrismaClient;

  namespace NodeJS {
    interface ProcessEnv {
      SESSION_SECRET: string;
    }
  }
}

export {};
