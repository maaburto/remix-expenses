import { z } from "zod";
import type {
  ErrorUserCredentialsForm,
  UserCredentialsFieldValidation,
} from "~/models/user";

const credentialsUserFormValidation = z
  .object({
    email: z
      .string()
      .min(1, {
        message: "Email should be real more than 1 letter",
      })
      .email({
        message: "Invalid email address",
      }),
    password: z.string().min(7, {
      message: "Invalid password. Must be at least 7 character.",
    }),
  })
  .required({
    email: true,
    password: true,
  });

type ValidateExpenseInputs = (
  data: UserCredentialsFieldValidation
) => ErrorUserCredentialsForm | null;
const validateCredentialsUserInputs: ValidateExpenseInputs = (
  data: UserCredentialsFieldValidation
) => {
  const validated = credentialsUserFormValidation.safeParse({
    email: data?.email,
    password: data?.password,
  });

  if (!validated.success) {
    const formatted = validated.error.format();
    return {
      email: formatted?.email?._errors || [],
      password: formatted?.password?._errors || [],
    };
  }

  return null;
};

export default validateCredentialsUserInputs;
