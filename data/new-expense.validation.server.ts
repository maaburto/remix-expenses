import { z } from "zod";
import type { UpdateExpense } from "~/models/expense";

type ExpenseFormField = Omit<UpdateExpense, "id" | "dateAdded">;

const expenseFormValidation = z
  .object({
    title: z
      .string({
        required_error: '"title" neeeds to be required!!',
      })
      .min(1, {
        message: 'The "title" it\'s required and must contain a real "title"',
      })
      .max(30),
    amount: z
      .number({
        required_error: 'The "title" neeeds to be required!!',
        invalid_type_error: "amount should be a number",
      })
      .min(1, {
        message: "The amount for expenses only be greather than 1",
      }),
    date: z.date(),
  })
  .required({
    amount: true,
    date: true,
    title: true,
  });

const validateExpenseInputs = (data: ExpenseFormField) => {
  const validated = expenseFormValidation.safeParse({
    title: data.title,
    amount: data.amount,
    date: data.date,
  });

  if (!validated.success) {
    const formatted = validated.error.format();
    return {
      title: formatted?.title?._errors || [],
      amount: formatted?.amount?._errors || [],
      date: formatted?.date?._errors || [],
    };
  }

  return null;
};

export default validateExpenseInputs;
