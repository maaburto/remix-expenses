import type { NewExpense, UpdateExpense } from "~/models/expense";
import { prisma } from "./database.server";

export async function addExpense(params: NewExpense, userId: string) {
  try {
    const createdExpense = await prisma.expense.create({
      data: {
        ...params,
        User: {
          connect: {
            id: userId,
          },
        },
      },
    });

    return createdExpense;
  } catch (err) {
    throw new Error("Failed to add expense");
  }
}

export async function fetchExpenses(userId: string) {
  try {
    const fetchedExpenses = await prisma.expense.findMany({
      where: {
        userId,
      },
      orderBy: {
        date: "desc",
      },
    });

    return fetchedExpenses;
  } catch (err) {
    throw new Error("Failed to get expenses");
  }
}

export async function fetchExpense(expenseId: string) {
  try {
    const fetchedExpense = await prisma.expense.findFirst({
      where: {
        id: expenseId,
      },
    });

    return fetchedExpense;
  } catch (err) {
    throw new Error("Failed to get expense");
  }
}

export async function updateExpense(
  expenseId: string,
  expenseData: UpdateExpense
) {
  try {
    const updated = await prisma.expense.update({
      data: expenseData,
      where: {
        id: expenseId,
      },
    });

    return updated;
  } catch (err) {
    throw new Error("Failed to update expense");
  }
}

export async function deleteExpense(expenseId: string) {
  try {
    const deleted = await prisma.expense.delete({
      where: {
        id: expenseId,
      },
    });

    return deleted;
  } catch (error) {
    throw new Error("Failed to delete expense");
  }
}
