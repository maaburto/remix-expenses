import { hash, compare } from "bcryptjs";
import { prisma } from "./database.server";
import type { SignUpData } from "~/models/user";
import { UserCustomError } from "~/models/user";
import { redirect } from "@remix-run/node";
import {
  createSessionByUserId,
  getSessionByUserId,
  default as sessionStorage,
} from "./cookie-session.server";
import { AuthFormMode } from "~/models/auth";

export async function signup(credentials: SignUpData) {
  try {
    const existingUser = await existsUser(credentials.email);

    if (existingUser) {
      const error = new Error(
        "A user with the provided email address exists already."
      );
      error.name = UserCustomError.EXISTING_USER;
      throw error;
    }

    const password = await hash(credentials.password, 12);
    const createdUser = await prisma.user.create({
      data: {
        email: credentials.email,
        password,
      },
    });

    return createdUser;
  } catch (err) {
    if (err instanceof Error) {
      if (err.name === UserCustomError.EXISTING_USER) {
        throw err;
      }

      throw new Error("Failed to create user");
    }
  }
}

export async function signin(credentials: SignUpData) {
  try {
    const existingUser = await existsUser(credentials.email);
    const badLoginErrorMessage =
      "Could not log you in, please check the provided credentials";

    if (!existingUser) {
      const error = new Error(badLoginErrorMessage);
      error.name = UserCustomError.UNSUCCESS_AUTHENTICATION;
      throw error;
    }

    const passwordCorrect = await compare(
      credentials.password,
      existingUser.password
    );

    if (!passwordCorrect) {
      const error = new Error(badLoginErrorMessage);
      error.name = UserCustomError.UNSUCCESS_AUTHENTICATION;
      throw error;
    }

    return existingUser;
  } catch (err) {
    if (err instanceof Error) {
      if (err.name === UserCustomError.UNSUCCESS_AUTHENTICATION) {
        throw err;
      }

      throw new Error("Failed on authentication process");
    }
  }
}

export async function existsUser(email: string) {
  try {
    const user = await prisma.user.findFirst({
      where: {
        email,
      },
    });

    return user;
  } catch (err) {
    throw err;
  }
}

export async function getUserFromSession(headerCookieValue: string) {
  const session = await getSessionByUserId(headerCookieValue);

  return (session.get("userId") as string | null) || null;
}

export async function createUserSession(userId: string, redirectPath: string) {
  try {
    const session = await createSessionByUserId(userId);

    return redirect(redirectPath, {
      headers: {
        "Set-Cookie": await sessionStorage.commitSession(session),
      },
    });
  } catch (err) {
    throw err;
  }
}

export async function destroyUserSession(
  headerCookieValue: string,
  redirectPath: string = "/"
) {
  const session = await getSessionByUserId(headerCookieValue);

  return redirect(redirectPath, {
    headers: {
      "Set-Cookie": await sessionStorage.destroySession(session),
    },
  });
}

// This is a function that acts like a Guard!
export async function requireUserSession(headerCookieValue: string) {
  const userId = await getUserFromSession(headerCookieValue);

  if (!userId) {
    // Using this bellow, catch boundary won't be triggered!
    throw redirect(`/auth?mode=${AuthFormMode.LOGIN}`);
  }

  return userId;
}
