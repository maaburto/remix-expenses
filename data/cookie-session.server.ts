import { createCookieSessionStorage } from "@remix-run/node";

const sessionStorage = createCookieSessionStorage({
  cookie: {
    secure: process.env.NODE_ENV === "production",
    secrets: [process.env.SESSION_SECRET],
    sameSite: "lax",
    maxAge: 30 * 24 * 60 * 60, // 30 days
    httpOnly: true,
  },
});

export async function createSessionByUserId(userId: string) {
  const session = await sessionStorage.getSession();
  session.set("userId", userId);

  return session;
}

export async function getSessionByUserId(headerCookieValue: string) {
  const session = await sessionStorage.getSession(
    headerCookieValue
  );

  return session;
}

export default sessionStorage;
