import type { LinksFunction, MetaFunction } from "@remix-run/node";
import type { FC } from "react";
import {
  Link,
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useCatch,
  useMatches,
} from "@remix-run/react";
import type {
  DocumentProps,
  ErrorBoundaryProps,
} from "~/models/custom-node.model";
import styles from "~/styles/shared.css";
import { Error } from "./components/util";

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "Remix Expenses",
  viewport: "width=device-width,initial-scale=1",
});

export const links: LinksFunction = () => [
  {
    rel: "stylesheet",
    href: styles,
  },
  {
    rel: "preconnect",
    href: "https://fonts.googleapis.com",
  },
  {
    rel: "preconnect",
    href: "https://fonts.gstatic.com",
    crossOrigin: "anonymous",
  },
  {
    rel: "stylesheet",
    href: "https://fonts.googleapis.com/css2?family=Rubik:wght@400;700&display=swap",
  },
];

const Document: FC<DocumentProps> = ({ title, children }) => {
  const matches = useMatches();
  const disabledJS = matches.some((match) => match.handle?.disableJS);

  return (
    <html lang='en'>
      <head>
        {title && <title>{title}</title>}
        <Meta />
        <Links />
      </head>
      <body>
        {children}
        <ScrollRestoration />
        {!disabledJS && <Scripts />}
        <LiveReload />
      </body>
    </html>
  );
};

export const CatchBoundary: FC = () => {
  const caughtResponse = useCatch();

  return (
    <Document title={caughtResponse.statusText}>
      <main>
        <Error title={caughtResponse.statusText}>
          <p>
            {caughtResponse.data?.message ||
              "Something went wrong. Please try again later."}
          </p>
          <p>
            Back to <Link to={"/"}>safety</Link>
          </p>
        </Error>
      </main>
    </Document>
  );
};

export const ErrorBoundary: FC<ErrorBoundaryProps> = ({ error }) => {
  return (
    <Document title={"An Error occured"}>
      <main>
        <Error title={"An Error occured"}>
          <p>
            {error?.message || "Something went wrong. Please try again later."}
          </p>
          <p>
            Back to <Link to={"/"}>safety</Link>
          </p>
        </Error>
      </main>
    </Document>
  );
};

export default function App() {
  return (
    <Document>
      <Outlet />
    </Document>
  );
}
