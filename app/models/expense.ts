import type { Prisma } from "@prisma/client";
import type { ErrorFormEntry } from "./form-field";

export interface IExpense {
  id: string;
  title: string;
  date: string | Date;
  amount: number;
}

export class Expense implements IExpense {
  id: string;
  title: string;
  date: string | Date;
  amount: number;

  constructor(data: IExpense) {
    this.id = data.id;
    this.title = data.title;
    this.date = data.date;
    this.amount = data.amount;
  }
}

export interface NewExpense extends Omit<Prisma.ExpenseCreateInput, "User"> {}
export interface UpdateExpense extends Prisma.ExpenseUpdateInput {}
export type ExpensesChart = Omit<Expense, "id" | "name">;

export interface ErrorNewExpenseForm extends ErrorFormEntry<NewExpense> {}
