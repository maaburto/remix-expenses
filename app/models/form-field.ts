export type ErrorFormEntry<T> = {
  [key in keyof T]: string[];
};
