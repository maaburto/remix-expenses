import type { ReactNode } from "react";

export type ChildrenNode = ReactNode | JSX.Element | JSX.Element[];

export interface ChildrenProps {
  children: ChildrenNode;
}

export interface ErrorBoundaryProps {
  error: Error;
}

export interface DocumentProps {
  title?: string;
  children: ChildrenNode;
}

export enum CacheControlValues {
  ONE_HOUR = "max-age=3600",
  NO_CACHE = "max-age=no-cache",
}
