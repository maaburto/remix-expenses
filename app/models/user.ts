import type { Prisma } from "@prisma/client";
import type { ErrorFormEntry } from "./form-field";

export interface IUser {
  id: string;
  email: string;
}

export class User implements IUser {
  id: string;
  email: string;

  constructor(user: IUser) {
    this.id = user.id;
    this.email = user.email;
  }
}

export type UserCredentialsFieldValidation = Omit<
  Prisma.UserUpdateInput,
  "expenses"
>;

export type SignUpData = Omit<Prisma.UserCreateInput, "id" | "expenses">;

export interface ErrorUserCredentialsForm
  extends ErrorFormEntry<UserCredentialsFieldValidation> {
  credentials?: string[];
}

export enum UserCustomError {
  EXISTING_USER = "EXISTING_USER",
  UNSUCCESS_AUTHENTICATION = "UNSUCCESS_AUTHENTICATION",
}
