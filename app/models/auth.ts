export enum AuthFormMode {
  LOGIN = "login",
  SIGN_UP = "signup",
}
