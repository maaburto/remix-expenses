import { Form, Link, useActionData } from "@remix-run/react";
import { FaLock, FaUserPlus } from "react-icons/fa";
import { ErrorsFormFields } from "~/components/ErrorsFormFields";
import { useSubmissionForm } from "~/hooks";
import { AuthFormMode } from "~/models/auth";
import type { ErrorUserCredentialsForm } from "~/models/user";
import useAuthParams from "./useAuthParams";

function AuthForm() {
  const { isSubmitting } = useSubmissionForm();
  const validationErrors = useActionData<ErrorUserCredentialsForm>();
  const { authMode, nextActionMode, submitBtnCaption, toogleBtnCaption } =
    useAuthParams(isSubmitting);

  return (
    <Form method='post' className='form' id='auth-form'>
      <div className='icon-img'>
        {authMode === AuthFormMode.LOGIN ? <FaLock /> : <FaUserPlus />}
      </div>
      <p>
        <label htmlFor='email'>Email Address</label>
        <input type='email' id='email' name='email' required />
      </p>
      <p>
        <label htmlFor='password'>Password</label>
        <input type='password' id='password' name='password' minLength={7} />
      </p>

      {validationErrors && (
        <ErrorsFormFields validationErrors={validationErrors}  />
      )}
      <div className='form-actions'>
        <button disabled={isSubmitting}>{submitBtnCaption}</button>
        <Link to={`?mode=${nextActionMode}`}>{toogleBtnCaption}</Link>
      </div>
    </Form>
  );
}

export default AuthForm;
