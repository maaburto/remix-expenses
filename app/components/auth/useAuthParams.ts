import { useSearchParams } from "@remix-run/react";
import { AuthFormMode } from "~/models/auth";

const useAuthParams = (isSubmitting: boolean) => {
  const [searchParams] = useSearchParams();
  const authMode = searchParams.get("mode") || AuthFormMode.LOGIN;

  let submitBtnCaption = null;

  if (isSubmitting) {
    submitBtnCaption =
      authMode === AuthFormMode.LOGIN
        ? "Authenticating..."
        : "Creating User...";
  } else {
    submitBtnCaption =
      authMode === AuthFormMode.LOGIN ? AuthFormMode.LOGIN : "Create User";
  }

  const toogleBtnCaption =
    authMode === AuthFormMode.LOGIN
      ? "Create a new user"
      : "Log in with existing user";
  const nextActionMode =
    authMode === AuthFormMode.LOGIN ? AuthFormMode.SIGN_UP : AuthFormMode.LOGIN;

  return {
    authMode,
    submitBtnCaption,
    toogleBtnCaption,
    nextActionMode,
    isSubmitting,
  };
};

export default useAuthParams;
