import { Link } from "@remix-run/react";

const NoExpenses = () => {
  return (
    <section id='no-expenses'>
      <h1>No expenses found</h1>
      <p>
        Start <Link to={"add"}>adding some</Link> today.
      </p>
    </section>
  );
};

export default NoExpenses;
