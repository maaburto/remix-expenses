import type { FC } from "react";
import type { ChildrenNode } from "~/models/custom-node.model";

interface ModalProps {
  children: ChildrenNode;
  onClose: () => void;
}

const Modal: FC<ModalProps> = ({ children, onClose }) => {
  return (
    <div className='modal-backdrop' onClick={onClose}>
      <dialog
        className='modal'
        open
        onClick={(event) => event.stopPropagation()}
      >
        {children}
      </dialog>
    </div>
  );
};

export default Modal;
