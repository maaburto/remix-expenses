import { FaExclamationCircle } from "react-icons/fa";
import type { FC } from "react";
import type { ChildrenNode } from "~/models/custom-node.model";

interface ErrorProps {
  title: string;
  children: ChildrenNode;
}

const Error: FC<ErrorProps> = ({ title, children }) => {
  return (
    <div className='error'>
      <div className='icon'>
        <FaExclamationCircle />
      </div>
      <h2>{title}</h2>
      {children}
    </div>
  );
};

export default Error;
