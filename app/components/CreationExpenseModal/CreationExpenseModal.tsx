import { useNavigate } from "@remix-run/react";
import { ExpenseForm } from "~/components/ExpenseForm";
import { Modal } from "~/components/util";

export default function CreationExpenseModal() {
  const navigate = useNavigate();

  const closeModalHandler = () => {
    navigate("..");
  };

  return (
    <Modal onClose={closeModalHandler}>
      <ExpenseForm />
    </Modal>
  );
}
