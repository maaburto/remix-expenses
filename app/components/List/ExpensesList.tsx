import type { FC } from "react";
import type { Expense } from "~/models/expense";
import { ExpenseListItem } from "../ItemList";

interface ExpensesListProps {
  expenses: Expense[];
  isDeleting: boolean;
  expenseIdToDelete: string | null;
  deleteExpenseItem: (expenseId: string) => void;
}

const ExpensesList: FC<ExpensesListProps> = ({
  expenses,
  isDeleting,
  deleteExpenseItem,
  expenseIdToDelete,
}) => {
  return (
    <ol id='expenses-list'>
      {expenses.map((expense) => (
        <li key={expense.id}>
          <ExpenseListItem
            id={expense.id}
            title={expense.title}
            amount={expense.amount}
            isSubmitting={isDeleting && expenseIdToDelete === expense.id}
            onDeleteItem={deleteExpenseItem}
          />
        </li>
      ))}
    </ol>
  );
};

export default ExpensesList;
