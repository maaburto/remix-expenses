import { Link } from "@remix-run/react";
import type { FC } from "react";

interface ExpenseListItemProps {
  id: string;
  title: string;
  amount: number;
  isSubmitting: boolean;
  onDeleteItem: (expenseId: string) => void;
}

const ExpenseListItem: FC<ExpenseListItemProps> = ({
  id,
  title,
  amount,
  isSubmitting,
  onDeleteItem,
}) => {
  if (isSubmitting) {
    return (
      <article className='expense-item locked'>
        <p>Deleting...</p>
      </article>
    );
  }

  return (
    <article className='expense-item'>
      <div>
        <h2 className='expense-title'>{title}</h2>
        <p className='expense-amount'>${amount.toFixed(2)}</p>
      </div>
      <menu className='expense-actions'>
        <button onClick={onDeleteItem.bind(null, id)}>Delete</button>
        <Link to={id}>Edit</Link>
      </menu>
    </article>
  );
};

export default ExpenseListItem;
