import { Form, Link, NavLink } from "@remix-run/react";
import type { FC } from "react";
import Logo from "../util/Logo";

interface MainHeaderProps {
  userId: string | null;
}

const MainHeader: FC<MainHeaderProps> = ({ userId }) => {
  return (
    <header id='main-header'>
      <Logo />
      <nav id='main-nav'>
        <ul>
          <li>
            <NavLink to='/'>Home</NavLink>
          </li>
          <li>
            <NavLink to='/pricing'>Pricing</NavLink>
          </li>
        </ul>
      </nav>
      <nav id='cta-nav'>
        <ul>
          <li>
            {!userId && (
              <Link to='/auth' className='cta'>
                Login
              </Link>
            )}

            {userId && (
              <Form method='post' id='logout-form' action="/logout">
                <button className='cta-alt'>Logout</button>
              </Form>
            )}
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default MainHeader;
