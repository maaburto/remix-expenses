import type { FC } from "react";

interface ErrorsFormFieldsProps {
  validationErrors: {
    [key: string]: string[];
  };
}

const ErrorsFormFields: FC<ErrorsFormFieldsProps> = ({ validationErrors }) => {
  return (
    <ul>
      {Object.values(validationErrors).map((validationErrors, indexErrors) =>
        validationErrors.map((error, indexError) => (
          <li key={`${indexErrors}${indexError}`}>{error}</li>
        ))
      )}
    </ul>
  );
};

export default ErrorsFormFields;
