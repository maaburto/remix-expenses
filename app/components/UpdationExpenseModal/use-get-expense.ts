import useGetExpenseMatches from "./use-get-expense-matches";

const useGetExpense = () => {
  const { expense, paramsExpenseId } = useGetExpenseMatches();

  return {
    expense,
    paramsExpenseId,
  };
};

export default useGetExpense;
