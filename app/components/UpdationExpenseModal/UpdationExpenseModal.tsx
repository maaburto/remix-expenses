import { useNavigate } from "@remix-run/react";
import { ExpenseForm } from "~/components/ExpenseForm";
import { Modal } from "~/components/util";
import useGetExpense from "./use-get-expense";

export default function UpdationExpenseModal() {
  const { expense, paramsExpenseId } = useGetExpense();
  const navigate = useNavigate();
  const invalidExpense = paramsExpenseId && !expense;

  const closeModalHandler = () => {
    navigate("/expenses");
  };

  return (
    <Modal onClose={closeModalHandler}>
      {!invalidExpense && (
        <ExpenseForm
          id={expense?.id}
          title={expense?.title}
          amount={expense?.amount}
          date={expense?.date}
        />
      )}
      {invalidExpense && <p>Invalid expense id.</p>}
    </Modal>
  );
}
