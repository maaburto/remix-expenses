import { useLoaderData, useParams } from "@remix-run/react";
import type { Expense } from "~/models/expense";

const useGetExpenseLoader = () => {
  const expense = useLoaderData<Expense>();
  const params = useParams<{ expenseId: string }>();
  const paramsExpenseId = params?.expenseId;

  return { expense, paramsExpenseId };
};

export default useGetExpenseLoader;
