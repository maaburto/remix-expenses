import { useMatches, useParams } from "@remix-run/react";
import type { Expense } from "~/models/expense";

const useGetExpenseMatches = () => {
  // Instead to use useLoaderData, useMatches will be used because on parent component (expenses.tsx)
  // Contains all expenses that even include the selected expense to pre-fill the values on ExpenseForm
  // const expense = useLoaderData<Expense>();

  // Optional if parent (route) or any sibling (route) contaisn data that fulfill
  // with expense that you want we could the useMatches hook
  const matches = useMatches();
  const params = useParams<{ expenseId: string }>();
  const paramsExpenseId = params?.expenseId;
  const expenses =
    (matches.find((match) => match.id === "routes/__app/expenses")
      ?.data as Expense[]) || [];
  const expense = expenses.find((expense) => expense.id === params?.expenseId);

  return { expense, paramsExpenseId };
};

export default useGetExpenseMatches;
