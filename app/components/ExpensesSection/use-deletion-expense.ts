import { useFetcher } from "@remix-run/react";
import { useState } from "react";

const useDeletionExpense = () => {
  const fetcher = useFetcher();
  const [expenseId, setExpenseId] = useState<string | null>(null);

  const setExpenseIdToDelete = (id: string | null) => {
    setExpenseId(id);
  };

  const deleteExpense = (id: string) => {
    fetcher.submit(null, {
      method: "delete",
      action: `/expenses/${id}`,
    });
  };

  const isDeleting = fetcher.state !== "idle";

  return {
    expenseIdToDelete: expenseId,
    setExpenseIdToDelete,
    deleteExpense,
    isDeleting,
  };
};

export default useDeletionExpense;
