import { useLoaderData } from "@remix-run/react";
import type { Expense } from "~/models/expense";
import { ExpensesList } from "../List";
import { NoExpenses } from "../NoExpenses";
import useDeletionExpense from "./use-deletion-expense";

const ExpensesSection = () => {
  const expenses = useLoaderData<Expense[]>();
  const hasExpenses = expenses && expenses.length > 0;
  const {
    expenseIdToDelete,
    setExpenseIdToDelete,
    deleteExpense,
    isDeleting,
  } = useDeletionExpense();

  const deleteExpenseItemHandler = (id: string) => {
    setExpenseIdToDelete(id);
    const proceedToDelete = confirm(
      "Are you sure? Do you want to delete this item?"
    );

    if (!proceedToDelete) {
      setExpenseIdToDelete(null);
      return;
    }

    deleteExpense(id);
  };

  if (!hasExpenses) return <NoExpenses />;

  return (
    <ExpensesList
      expenses={expenses}
      isDeleting={isDeleting}
      expenseIdToDelete={expenseIdToDelete}
      deleteExpenseItem={deleteExpenseItemHandler}
    />
  );
};

export default ExpensesSection;
