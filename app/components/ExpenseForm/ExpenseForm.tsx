import { Form, Link, useActionData } from "@remix-run/react";
import type { FC } from "react";
import { ErrorsFormFields } from "~/components/ErrorsFormFields";
import { useSubmissionForm } from "~/hooks";
import type { ErrorNewExpenseForm, Expense } from "~/models/expense";

interface ExpenseFormProps extends Partial<Expense> {}

const ExpenseForm: FC<ExpenseFormProps> = ({ id, date, title, amount }) => {
  // const submit = useSubmit();
  const validationErrors = useActionData<ErrorNewExpenseForm>();
  const today = new Date().toISOString().slice(0, 10); // yields something like 2023-09-10
  const { isSubmitting } = useSubmissionForm();

  const expenseDate = date ? String(date).slice(0, 10) : "";

  // const submitHandler = (event: FormEvent<HTMLFormElement>) => {
  //   event.preventDefault();
  //   // This is used in order to programatically
  //   // submit the form as web standard form does
  //   submit(event.currentTarget, {
  //     method: "post",
  //   });
  // };

  return (
    <Form method={id ? "patch" : "post"} className='form' id='expense-form'>
      <p>
        <label htmlFor='title'>Expense Title</label>
        <input
          type='text'
          id='title'
          name='title'
          required
          maxLength={30}
          defaultValue={title || ""}
        />
      </p>

      <div className='form-row'>
        <p>
          <label htmlFor='amount'>Amount</label>
          <input
            type='number'
            id='amount'
            name='amount'
            min='0'
            step='0.01'
            defaultValue={amount || ""}
            required
          />
        </p>
        <p>
          <label htmlFor='date'>Date</label>
          <input
            type='date'
            id='date'
            name='date'
            max={today}
            required
            defaultValue={expenseDate}
          />
        </p>
      </div>

      {validationErrors && (
        <ErrorsFormFields validationErrors={validationErrors} />
      )}
      <div className='form-actions'>
        <button disabled={isSubmitting}>
          {isSubmitting ? "Saving..." : "Save Expense"}
        </button>
        <Link to={".."}>Cancel</Link>
      </div>
    </Form>
  );
};

export default ExpenseForm;
