import { useTransition as useNavigation } from "@remix-run/react";

const useSubmissionForm = () => {
  const navigation = useNavigation();
  const isSubmitting = navigation.state !== "idle";

  return { isSubmitting, submission: navigation.submission };
};

export default useSubmissionForm;
