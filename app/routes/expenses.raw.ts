import { json } from "@remix-run/node";
import type { LoaderFunction } from "@remix-run/node";
import { fetchExpenses } from "data/expenses.server";
import { requireUserSession } from "data/auth.server";

export const loader: LoaderFunction = async ({ request }) => {
  const userId = await requireUserSession(
    String(request.headers.get("Cookie"))
  );

  const expeneses = await fetchExpenses(userId);
  return json(expeneses);
};
