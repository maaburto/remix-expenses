import { CreationExpenseModal } from "~/components/CreationExpenseModal";
import { json, redirect } from "@remix-run/node";
import type { ActionFunction } from "@remix-run/node";
import { addExpense } from "data/expenses.server";
import type { NewExpense } from "~/models/expense";
import validateExpenseInputs from "data/new-expense.validation.server";
import { requireUserSession } from "data/auth.server";

export default function NewExpensesPage() {
  return <CreationExpenseModal />;
}

export const action: ActionFunction = async ({ request }) => {
  const userId = await requireUserSession(
    String(request.headers.get("Cookie"))
  );

  const formData = await request.formData();
  const preparementExpense: NewExpense = {
    title: formData.get("title") as string,
    amount: +(formData.get("amount") as unknown as number),
    date: new Date(formData.get("date") as unknown as string),
  };

  const validatedInputs = validateExpenseInputs(preparementExpense);
  if (validatedInputs) {
    return json(validatedInputs, {
      status: 400,
    });
  }

  await addExpense(preparementExpense, userId);

  return redirect("/expenses");
};
