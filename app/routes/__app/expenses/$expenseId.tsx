import type { ActionFunction, MetaFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { deleteExpense, updateExpense } from "data/expenses.server";
import validateExpenseInputs from "data/new-expense.validation.server";
import { UpdationExpenseModal } from "~/components/UpdationExpenseModal";
import type { Expense, UpdateExpense } from "~/models/expense";

export default function ExpensePage() {
  return <UpdationExpenseModal />;
}

// Optional if parent or any sibling route contain data that fullfill
// that you want we could the useMatches hook
// export const loader: LoaderFunction = ({ params }) => {
//   const expenseId = params.expenseId as string;
//   const expense = fetchExpense(expenseId);

//   return expense;
// };

export const action: ActionFunction = async ({ params, request }) => {
  const method = request.method;
  const expenseId = params.expenseId as string;

  if (method === "PATCH") {
    const formData = await request.formData();
    const formDataEntries = Object.fromEntries(formData);
    const expenseData: UpdateExpense = {
      title: String(formDataEntries["title"]),
      amount: +formDataEntries["amount"],
      date: new Date(String(formDataEntries["date"])),
    };
    const validatedInputs = validateExpenseInputs(expenseData);
    if (validatedInputs) {
      return json(validatedInputs, {
        status: 400,
      });
    }
    await updateExpense(expenseId, expenseData);

    return redirect("/expenses");
  }

  if (method === "DELETE") {
    await deleteExpense(expenseId);

    return { deleteId: expenseId };
  }
};

export const meta: MetaFunction = ({ params, parentsData }) => {
  const expense = parentsData["routes/__app/expenses"].find(
    (expense: Expense) => expense.id === params.expenseId
  ) as Expense;
  return {
    title: expense.title,
    description: "Update expense",
  };
};
