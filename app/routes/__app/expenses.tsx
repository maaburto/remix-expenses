import type {
  HeadersFunction,
  LoaderFunction,
  MetaFunction} from "@remix-run/node";
import {
  json
} from "@remix-run/node";
import { Link, Outlet } from "@remix-run/react";
import { requireUserSession } from "data/auth.server";
import { fetchExpenses } from "data/expenses.server";
import { FaDownload, FaPlus } from "react-icons/fa";
import { ExpensesSection } from "~/components/ExpensesSection";
import { CacheControlValues } from "~/models/custom-node.model";

export default function ExpensesLayoutPage() {
  return (
    <main>
      <Outlet />
      <section>
        <section id='expenses-actions'>
          <Link to={"add"}>
            <FaPlus />
            <span>Add Expense</span>
          </Link>

          <a href='/expenses/raw'>
            <FaDownload />
            <span>Load Raw Data</span>
          </a>
        </section>

        <ExpensesSection />
      </section>
    </main>
  );
}

export const loader: LoaderFunction = async ({ request }) => {
  const userId = await requireUserSession(
    String(request.headers.get("Cookie"))
  );

  const expenses = await fetchExpenses(userId);
  return json(expenses, {
    headers: {
      "Cache-Control": "max-age=3",
    },
  });
};

export const meta: MetaFunction = () => ({
  title: "Expeneses",
});

export const headers: HeadersFunction = ({ loaderHeaders }) => {
  // loaderHeaders will gather all headers generated
  // in expenses response with Cache-Control instead a parentHeaders

  return {
    "Cache-Control":
      loaderHeaders.get("Cache-Control") || CacheControlValues.NO_CACHE,
  };
};
