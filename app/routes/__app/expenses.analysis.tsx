import { Chart } from "~/components/Chart";
import { ExpenseStatistics } from "~/components/ExpenseStatistics";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { fetchExpenses } from "data/expenses.server";
import { useCatch, useLoaderData } from "@remix-run/react";
import type { Expense } from "~/models/expense";
import type { FC } from "react";
import { Error } from "~/components/util";
import { requireUserSession } from "data/auth.server";

export default function AnalysisExpensesPage() {
  const expenses = useLoaderData<Expense[]>();

  return (
    <section id='expenses-analysis'>
      <Chart expenses={expenses} />
      <ExpenseStatistics expenses={expenses} />
    </section>
  );
}

export const loader: LoaderFunction = async ({ request }) => {
  const userId = await requireUserSession(
    String(request.headers.get("Cookie"))
  );

  const expeneses = await fetchExpenses(userId);

  if (!expeneses || expeneses.length === 0) {
    throw json(
      {
        message: "Could not load expenses for the requested analysis.",
      },
      {
        status: 404,
        statusText: "Expenses not found",
      }
    );
  }

  return expeneses;
};

export const CatchBoundary: FC = () => {
  const caughtResponse = useCatch();

  return (
    <main>
      <Error title={caughtResponse.statusText}>
        <p>
          {caughtResponse.data?.message ||
            "Something went wrong - could not load expenses."}
        </p>
      </Error>
    </main>
  );
};
