import type { HeadersFunction, LinksFunction, LoaderFunction } from "@remix-run/node";
import { Outlet, useLoaderData } from "@remix-run/react";
import { getUserFromSession } from "data/auth.server";
import MainHeader from "~/components/navigation/MainHeader";
import marketingStyles from "~/styles/marketing.css";

export default function MarketingAppLayout() {
  const userId = useLoaderData<string | null>();

  return (
    <>
      <MainHeader userId={userId} />
      <Outlet />
    </>
  );
}

export const loader: LoaderFunction = ({ request }) => {
  return getUserFromSession(String(request.headers.get("Cookie")));
};

export const links: LinksFunction = () => [
  {
    rel: "stylesheet",
    href: marketingStyles,
  },
];

export const headers: HeadersFunction = () => ({
  "Cache-Control": "max-age=3600", // 60 minutes
});
