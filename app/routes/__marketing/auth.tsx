import type {
  ActionFunction,
  HeadersFunction,
  LinksFunction,
} from "@remix-run/node";
import { json } from "@remix-run/node";
import { createUserSession, signin, signup } from "data/auth.server";
import validateCredentialsUserInputs from "data/credential-validation.server";
import AuthForm from "~/components/auth/AuthForm";
import { AuthFormMode } from "~/models/auth";
import authStyles from "~/styles/auth.css";
import type { ErrorUserCredentialsForm, SignUpData, User } from "~/models/user";
import { UserCustomError } from "~/models/user";
import { CacheControlValues } from "~/models/custom-node.model";

export default function AuthPage() {
  return (
    <main>
      <AuthForm />
    </main>
  );
}

export const action: ActionFunction = async ({ request }) => {
  const searchParams = new URL(request.url).searchParams;
  const authMode = searchParams.get("mode") || AuthFormMode.LOGIN;
  const formData = await request.formData();
  const credentialEntrieObj = Object.fromEntries(formData);
  const credentials: SignUpData = {
    email: String(credentialEntrieObj["email"]),
    password: String(credentialEntrieObj["password"]),
  };

  const validatedInputsWithErrors = validateCredentialsUserInputs(credentials);
  if (validatedInputsWithErrors) {
    return json(validatedInputsWithErrors, {
      status: 400,
    });
  }

  try {
    let user: User | undefined;
    switch (authMode) {
      case AuthFormMode.LOGIN:
        user = await signin(credentials);
        break;

      case AuthFormMode.SIGN_UP:
        user = await signup(credentials);
        break;

      default:
        throw json(
          {
            message: "Invalid authenticaton request",
          },
          {
            status: 400,
          }
        );
    }

    if (user) {
      return createUserSession(user.id, "/expenses");
    }

    // await new Promise((resolve) => {
    //   setTimeout(() => {
    //     resolve(null);
    //   }, 2400);
    // });
  } catch (err) {
    if (err instanceof Error) {
      if (
        [
          UserCustomError.EXISTING_USER,
          UserCustomError.UNSUCCESS_AUTHENTICATION,
        ].includes(err.name as UserCustomError)
      ) {
        const customError: ErrorUserCredentialsForm = {
          credentials: [err.message],
        };
        return json(customError, {
          status: err.name === UserCustomError.EXISTING_USER ? 422 : 401,
        });
      }
    }
  }
  return null;
};

export const links: LinksFunction = () => [
  {
    rel: "stylesheet",
    href: authStyles,
  },
];

export const headers: HeadersFunction = ({ parentHeaders }) => {
  return {
    "Cache-Control":
      parentHeaders.get("Cache-Control") || CacheControlValues.NO_CACHE,
  };
};
